import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// greeting.test.js​
describe('greeting()', () => {
  it('says hello', () => {
    expect(greeting('Jest')).toBe('Hello, Jest!')
  });
});
